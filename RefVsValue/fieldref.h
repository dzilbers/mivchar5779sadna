#pragma once

using Name = char[12];
using Date = char[8];
using uint = unsigned int;

using Predicate = bool(*)(void*);

#pragma pack(1)
class Sector {
protected:
	uint sectorNr;
	char rawData[1020];
public:
	void print() {}
};

class VolumeHeader : public Sector {
public:
	Name& ownerName = reinterpret_cast<Name&>(rawData[12]);
	Name& diskName = reinterpret_cast<Name&>(rawData[0]);
	void print() {}
};
