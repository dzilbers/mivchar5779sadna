#include<iostream>
#include<cstring>
#include "fieldref.h"

using namespace std;

struct Student {
	int id;
	char firstName[20];
	char lastName[30];
	// ...
};

const Student& createStudent(int id, char fname[], char lname[]) {
	//Student* temp = new Student();
	Student temp;
	int i;
	temp.id = id;
	for (i = 0; i < 20; i++)
		temp.firstName[i] = fname[i];
	for (i = 0; i < 30; i++)
		temp.lastName[i] = lname[i];
	return temp;
}

int main() {
	cout << "Sector size: " << sizeof Sector << ", VHD size: " << sizeof VolumeHeader << endl;
	Student elisheva;

	int i;
	int& moreI = i;

	VolumeHeader vhd;

	vhd.Sector::print();



	char fname[20], lname[30];
	strncpy(fname, "Elisheva", 19);
	strncpy(lname, "Maimon", 19);
	Student ss = createStudent(345, fname, lname);

	elisheva = ss;
	//createStudent(345, fname, lname) = elisheva;
	// ...
	//delete &ss;

	using pStudent = Student*;

	pStudent* studs = new pStudent[10];
	pStudent** pStuds = new pStudent*;
	*pStuds = new pStudent[10];

	char* str = new char[10];

	return 0;
}
