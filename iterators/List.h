#pragma once
#include "iterators.h"

//------------------------------------------------
// Class List - Arbitrary size Lists
// Permits insertion and removal only from the front of the List
//------------------------------------------------
template <typename T>
class List {
protected:
	//--------------------------------------------
	// inner class Node a single element for the Nodeed List
	//--------------------------------------------
	class Node {
		T _value;
		Node* _next;
	public:
		// constructor
		Node(T, Node*);
		Node(const Node&);
		// data areas
		const T& value() const; // getter only
		Node* next() const;  // getter
		void  next(Node*);   // setter
	}; //end of class Node

public:

	class Iterator : public ForwardIterator<Node, T> {
		using typename ForwardIterator<Node, T>::ValueType;
		using typename ForwardIterator<Node, T>::Pointer;
		using typename ForwardIterator<Node, T>::Reference;
		using ForwardIterator<Node, T>::_p;
	public:
		Iterator(Pointer p) : ForwardIterator<Node, T>(p) {}
		void advance() override { 
			_p = _p->next();
		};
		Reference operator*() const {
			return _p->value();
		}
		bool operator==(const Iterator& rhs) const { 
			return ForwardIterator<Node, T>::operator==(rhs); 
		}
		bool operator!=(const Iterator& rhs) const {
			return ForwardIterator<Node, T>::operator!=(rhs);
		}
		Iterator& operator++() { advance(); return *this; }
	};

	// constructors
	List();
	List(const List&);
	List(List&&);
	~List();
	// operations
	void add(const T& value);
	const T& firstElement() const;
	bool search(const T&) const;
	bool isEmpty() const;
	void removeFirst();
	void clear();

	Iterator begin() const { return Iterator(_head); }
	Iterator end() const { return Iterator(nullptr); }

protected:
	// data field
	Node* _head;
};

//------------------------------------------------
// class Node implementation
//------------------------------------------------
template <typename T>
List<T>::Node::Node(T val, Node* nxt) : _value(val), _next(nxt) {}

template <typename T>
List<T>::Node::Node(const Node& src) : _value(src._value), _next(src._next) {}
template <typename T>
const T& List<T>::Node::value() const { return _value; }
template <typename T>
auto List<T>::Node::next() const -> Node* { return _next; }
template <typename T>
void List<T>::Node::next(Node* nxt) { _next = nxt; }

//--------------------------------------------
// class List implementation
//--------------------------------------------
template <typename T>
List<T>::List() : _head(nullptr) {}

template <typename T>
List<T>::List(const List &other) {
	Node *src, *trg;
	if (other._head == nullptr)
		_head = nullptr;
	else {
		_head = new Node(other._head->value(), nullptr);
		if (_head == nullptr)
			throw "failed in memory allocation";

		src = other._head;
		trg = _head;
		while (src->next() != nullptr) {
			trg->next(new Node(src->next()->value(), nullptr));
			if (trg->next() == nullptr)
				throw "failed in memory allocation";
			src = src->next();
			trg = trg->next();
		}
	}
}

template <typename T>
List<T>::List(List &&other) {
	// Keep the temp list is permanent
	_head = other._head;
	// Avoid destructing it in the temporary object
	other._head = nullptr;
}

template <typename T>
List<T>::~List() {
	clear();
}

template <typename T>
void List<T>::clear() {
	// empty all elements from the List
	Node* next;
	for (Node* p =_head; p != nullptr; p = next) {
		// delete the element pointed to by p
		next = p->next();
		p->next(nullptr);
		delete p;
	}
	// mark that the List contains no elements 
	_head = nullptr;
}

template <typename T>
bool List<T>::isEmpty() const {
	// test to see if the List is empty
	// List is empty if the_head pointer is null
	return _head == nullptr;
}

template <typename T>
void List<T>::add(const T& val) {
	//Add a new value to the front of a Nodeed List
	_head = new Node(val,_head);
	if (_head == nullptr)
		throw "failed in memory allocation";
}

template <typename T>
const T& List<T>::firstElement() const {
	// return first value in List
	if (isEmpty())
		throw "the List is empty, no first Element";
	return _head->value();
}

template <typename T>
bool List<T>::search(const T &val) const {
	// loop to test each element
	//for (Node* p =_head; p != nullptr; p = p->next())
	//	if (val == p->value())
	//		return true;
	//// not found
	//return false;
	for (auto it = begin(); it != end(); ++it)
		if (val == *it)
			return true;
	// not found
	return false;

}

template <typename T>
void List<T>::removeFirst() {
	// make sure there is a first element
	if (isEmpty())
		throw "the List is empty, no Elements to remove";
	// save pointer to the removed node
	Node* p =_head;
	// reassign the first node
	_head = p->next();
	p->next(nullptr);
	// recover memory used by the first element
	delete p;
}

