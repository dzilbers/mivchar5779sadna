#pragma once

template <typename I>
I partition(I begin, I end,
	int(*comparator)(typename I::Reference, typename I::Reference)) {
	return begin();
}

template <typename I>
void sort(I begin, I end,
	int(*comparator)(typename I::Reference, typename I::Reference)) {
	I temp = partition(begin, end, comparator);
}
