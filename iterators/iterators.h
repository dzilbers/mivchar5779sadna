#pragma once
#include<iterator>

// Read only iterator
template <typename T, typename R>
class ForwardIterator {
public:
	using ValueType = R;
	using Pointer = const T * ;
	using Reference = const R & ;
protected:
	Pointer _p;
	virtual void advance() = 0;
public:
	ForwardIterator(Pointer p) : _p(p) {}
	Reference operator*() const { return *_p; }
	bool operator==(const ForwardIterator& rhs) const { return _p == rhs._p; }
	bool operator!=(const ForwardIterator& rhs) const { return _p != rhs._p; }
	ForwardIterator& operator++() { advance(); return *this; }
};

// Read only iterator
template <typename T, typename R>
class BidirIterator : public ForwardIterator<T, R> {
public:
	using typename ForwardIterator<T, R>::ValueType;
	using typename ForwardIterator<T, R>::Pointer;
	using typename ForwardIterator<T, R>::Reference;
	using ForwardIterator<T, R>::_p;
	using ForwardIterator<T, R>::advance;
public:
	BidirIterator(Pointer p) : ForwardIterator<T, R>(p) {}
	virtual void revert() = 0;
	Reference operator*() const { return *_p; }
	bool operator==(const BidirIterator& rhs) const { return _p == rhs._p; }
	bool operator!=(const BidirIterator& rhs) const { return _p != rhs._p; }
	BidirIterator& operator++() { advance(); return *this; }
	BidirIterator& operator--() { revert(); return *this; }
};
