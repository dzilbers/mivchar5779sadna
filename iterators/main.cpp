#include <iostream>
#include "List.h"
#include <ctime>
#include "funcs.h"

using namespace std;

int main() {
	List<int> l1;
	cout << "Empty: " << l1.isEmpty() << endl;
	srand(time(NULL));
	for (int i = 10; i >= 0; --i)
		l1.add(rand() % 100 + 1);
	cout << "Empty: " << l1.isEmpty() << endl;
	cout << "first: " << l1.firstElement() << endl;
	cout << "List: ";
	for (auto it = l1.begin(); it != l1.end(); ++it)
		cout << *it << " ";
	cout << endl;
	l1.removeFirst();
	cout << "first: " << l1.firstElement() << endl;

	int n;
	cout << "Enter a number to search: ";
	cin >> n;
	cout << (l1.search(n) ? "Found" : "Not found") << endl;

	cout << "Enter a number to search: ";
	cin >> n;
	cout << (l1.search(n) ? "Found" : "Not found") << endl;

	sort(l1.begin(), l1.end(), [](const int& p1, const int& p2) { return p2 - p1; });

	return 0;
}