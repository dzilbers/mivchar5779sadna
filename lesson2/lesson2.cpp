// lesson2.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
#include <iostream>
using namespace std;

using PtrOper = int(*)(int, int);

int add(int a, int b) { return a + b; }
int sub(int a, int b) { return a - b; }

PtrOper subtract = sub;

int oper(int x, int y, PtrOper func) {
	int g = func(x, y); return g;
	return func(x, y);
}

struct Student {
	int id;
	char firstName[20];
	char lastName[30];
	// ...
};
Student& createStudent(int id, char fname[], char lname[]) {
	Student temp;
	int i;
	temp.id = id;
	for (i = 0; i < 20; i++)
		temp.firstName[i] = fname[i];
	for (i = 0; i < 30; i++)
		temp.lastName[i] = lname[i];
	return temp;
}
int main() {
	int i = 8;
	bool& b = reinterpret_cast<bool&>(i);
	Student elisheva;
	char fname[20], lname[30];
	strcpy(fname, "Elisheva");
	strcpy(lname, "Maimon");
	elisheva = createStudent(345, fname, lname);
	// ...
	return 0;


	int m = oper(7, 5, add);
	cout << m << endl;    // output: 12
	int n = oper(20, m, subtract);
	cout << n << endl;    // output: 8
	int k = oper(20, m, sub);
	cout << k << endl;    // output: 8
	return 0;
}

