#pragma once

template <typename T, typename U>
class Daddy {
public:
	class InternalClass {};
protected:
	T field1;
	U field2;
};