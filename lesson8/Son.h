#pragma once
#include<iostream>
#include "Daddy.h"

using namespace std;

//template <typename V, typename W> class Son;
//template <typename T, typename U> ostream& operator << (ostream&, Son<T, U>);

template <typename V, typename W>
class Son : public Daddy<V, W> {
	using Daddy<V, W>::field1;
	using Daddy<V, W>::field2;
	using InternalClass = Daddy<V, W>::InternalClass;
protected:
	V field3;
	InternalClass field4;
public:
	Son(V p1, W p2, V p3) {
		field1 = p1;
		field2 = p2;
		field3 = p3;
	}

	//friend ostream& operator << (ostream& out, Son o) {
	//	out << o.field1 << ", " << o.field2 << ", " << o.field3 << endl;
	//	return out;
	//}
	
	friend ostream& operator << <> (ostream&, Son);
};

template <typename T, typename U>
ostream& operator<< (ostream& out, Son<T, U> o) {
	out << o.field1 << ", " << o.field2 << ", " << o.field3 << endl;
	return out;
}
