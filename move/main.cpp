#include <iostream>

using namespace std;

class Rect {
	int width; int length;
	char* name;
public:
	Rect(int = 0, int = 0);
	Rect(const Rect&);
	Rect& operator=(const Rect&);
	Rect(Rect&&);
	Rect& operator=(Rect&&);
};

Rect::Rect(const Rect& other) {
	width = other.width; length = other.length;
	int len = strlen(other.name);
	name = new char[len + 1];
	strncpy(name, other.name, len);
	name[len] = '\0';
}

Rect::Rect(Rect&& other) {
	width = other.width; length = other.length;
	name = other.name;
	other.name = nullptr;
}

Rect& Rect::operator=(const Rect& rhs) {
	if (this == &rhs) return;
	width = rhs.width; length = rhs.length;
	if (name != rhs.name) {
		delete[] name;
		int len = strlen(rhs.name);
		name = new char[len + 1];
		strncpy(name, rhs.name, len);
	}
}

Rect& Rect::operator=(Rect&& other) {
	width = other.width; length = other.length;
	name = other.name;
	other.name = nullptr;
}

int main(void) {

	return 0;
}