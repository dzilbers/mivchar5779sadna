#include<iostream>
#include<array>
#include<algorithm>
#include<vector>
#include<iterator>
#include<stack>
#include<queue>
#include<forward_list>
#include<list>

using namespace std;

int main(void) {
	/*int size;
	cout << "Enter array size: ";
	cin >> size;*/

	array<int, 5> arr { 5,3,7,5,8 };
	sort(arr.begin(), arr.end());

	vector<int> v1{ 4,7,89,32,7,5,4,6,8 };
	vector<int> v2{ 7,76,2,65,87,43,6,3};

	sort(v1.begin(), v1.end());
	sort(v2.begin(), v2.end());

	vector<int> v3(v1.size() + v2.size() + 6, 99);
	vector<int>::iterator it = merge(v1.begin(), v1.end(), v2.begin(), v2.end(), v3.begin());
	for_each(v3.begin(), v3.end(), [](int n) { cout << n << " "; });
	cout << endl;
	cout << *it << " " << it - v3.begin() << endl;

	vector<double> * ptrVect = new vector<double>();

	return 0;
}